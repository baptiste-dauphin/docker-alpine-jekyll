FROM alpine:3.10
RUN apk add "ruby>=2.4.0" gcc make
RUN ruby -v \
	gem  -v \
	gcc  -v \
	g++  -v \
	make -v
RUN echo 'Install Jekyll and bundler gems' \
	gem install jekyll bundler
ENTRYPOINT ["jekyll"]